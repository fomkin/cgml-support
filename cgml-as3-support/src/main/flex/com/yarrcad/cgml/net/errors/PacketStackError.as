package com.yarrcad.cgml.net.errors {

public class PacketStackError extends Error {

    public static const NO_PACKETS_AVALIABLE:String = "No packets avaliable";

    public function PacketStackError(message:String) {
        super(message);
    }
}

}
