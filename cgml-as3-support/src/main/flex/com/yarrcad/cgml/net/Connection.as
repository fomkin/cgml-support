package com.yarrcad.cgml.net {

import com.yarrcad.cgml.net.events.ConnectionEvent;

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IOErrorEvent;
import flash.events.ProgressEvent;
import flash.events.SecurityErrorEvent;
import flash.net.Socket;
import flash.utils.ByteArray;

[Event(type="com.yarrcad.cgml.net.events.ConnectionEvent", name="packet")]
[Event(name="securityError", type="flash.events.SecurityErrorEvent")]
[Event(name="ioError", type="flash.events.IOErrorEvent")]
[Event(name="connect", type="flash.events.Event")]
[Event(name="close", type="flash.events.Event")]

public class Connection extends EventDispatcher {

    private var socket:Socket;
    private var packetStack:PacketStack;

    public function Connection() {
        socket = new Socket();
        socket.addEventListener(ProgressEvent.SOCKET_DATA, socket_socketDataHandler)
        socket.addEventListener(Event.CONNECT, forwardEvent);
        socket.addEventListener(IOErrorEvent.IO_ERROR, forwardEvent);
        socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, forwardEvent);
        socket.addEventListener(Event.CLOSE, forwardEvent);

        packetStack = new PacketStack();
    }

    public function connect(host:String, port:int):void {
        socket.connect(host, port);
    }

    public function get connected():Boolean {
        return socket.connected;
    }

    public function sendBytes(body:ByteArray):void {
        body.position = 0;
        socket.writeUnsignedInt(body.length);
        socket.writeBytes(body);
        socket.flush();
    }

    public function close():void {
        if (socket.connected)
            socket.close();
        dispatchEvent(new Event(Event.CLOSE));
    }

    private function forwardEvent(event:Event):void {
        dispatchEvent(event.clone());
    }

    private function socket_socketDataHandler(event:ProgressEvent):void {
        packetStack.processInput(socket);
        while (packetStack.size > 0) {
            dispatchEvent(new ConnectionEvent(ConnectionEvent.PACKET, packetStack.take()));
        }
    }
}

}
