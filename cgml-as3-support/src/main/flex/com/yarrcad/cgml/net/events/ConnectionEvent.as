package com.yarrcad.cgml.net.events {

import flash.events.Event;
import flash.utils.ByteArray;

public class ConnectionEvent extends Event {

    public static const PACKET:String = "packet";

    public var bytes:ByteArray;

    public function ConnectionEvent(type:String, bytes:ByteArray, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);
        this.bytes = bytes;
    }
}

}
