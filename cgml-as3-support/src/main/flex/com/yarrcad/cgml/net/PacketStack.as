package com.yarrcad.cgml.net {

import com.yarrcad.cgml.net.errors.PacketStackError;

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.utils.ByteArray;
import flash.utils.IDataInput;

public class PacketStack extends EventDispatcher {

    private var packets:Vector.<ByteArray>;
    private var knownPacketLength:int = -1;
    private var buffer:ByteArray;

    public function PacketStack() {
        packets = new Vector.<ByteArray>();
        buffer = new ByteArray();
    }

    public function processInput(input:IDataInput):void {
        if (knownPacketLength > -1) {

            // We already know packet length. So lets read out.
            const assumeLength:int = knownPacketLength - buffer.length;
            const toRead:int = assumeLength > input.bytesAvailable
                    ? input.bytesAvailable
                    : assumeLength;

            if (toRead > 0)
                input.readBytes(buffer, buffer.length, toRead);

            // Check packet length
            if (buffer.length >= knownPacketLength) {
                // Yeha! Packet complete.
                // Call user handler and reset the buffer.
                buffer.position = 0;
                packets.push(buffer);
                buffer = new ByteArray();
                knownPacketLength = -1;
                processInput(input);
            }
        } else if (input.bytesAvailable >= 4) {
            // Can read length of packet
            knownPacketLength = input.readUnsignedInt();
            processInput(input);
        }
    }

    [Bindable("sizeChange")]

    public function get size():uint {
        return packets.length;
    }

    public function take():ByteArray {
        if (packets.length > 0) {
            var result:ByteArray = packets.shift();
            dispatchEvent(new Event("sizeChange"));
            return result;
        }
        else throw new PacketStackError(PacketStackError.NO_PACKETS_AVALIABLE);
    }
}

}
