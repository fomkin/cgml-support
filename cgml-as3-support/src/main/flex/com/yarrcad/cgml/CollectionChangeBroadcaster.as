package com.yarrcad.cgml {
import flash.events.IEventDispatcher;

import mx.collections.ArrayCollection;
import mx.events.CollectionEvent;
import mx.events.PropertyChangeEvent;

public class CollectionChangeBroadcaster {

  private var target:IEventDispatcher;
  private var collection:ArrayCollection;
  private var name:String;

  public function CollectionChangeBroadcaster(name:String,  target:IEventDispatcher, collection:ArrayCollection) {

    this.target = target;
    this.collection = collection;
    this.name = name;

    if (collection != null)
      collection.addEventListener(CollectionEvent.COLLECTION_CHANGE, collectionChangeHandler);

    target.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, propertyChangeHanlder);
  }

  private function propertyChangeHanlder(event:PropertyChangeEvent):void {
    if (event.source != this && event.property == name) {
      // Remove old handler
      if (collection != null)
        collection.removeEventListener(CollectionEvent.COLLECTION_CHANGE, collectionChangeHandler);
      // Resolve a property and add new handler
      collection = target[name];
      collection.addEventListener(CollectionEvent.COLLECTION_CHANGE, collectionChangeHandler);
    }
  }

  private function collectionChangeHandler(event:CollectionEvent):void {
    target.dispatchEvent(PropertyChangeEvent.createUpdateEvent(this, name, null, collection));
  }

}

}
