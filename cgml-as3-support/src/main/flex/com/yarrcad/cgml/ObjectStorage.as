/*
 * This file is part of the CGML Compiler
 *
 * Copyright(c) 2010 Aleksey Fomkin <aleksey.fomkin@gmail.com>
 * http://bitbucket.org/yelbota/cgml-compiler
 *
 * This file may be licensed under the terms of of the
 * GNU General Public License Version 3 (the ``GPL'').
 *
 * Software distributed under the License is distributed
 * on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the GPL for the specific language
 * governing rights and limitations.
 *
 * You should have received a copy of the GPL along with this
 * program. If not, go to http://www.gnu.org/licenses/gpl.html
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */
package com.yarrcad.cgml {

import flash.utils.Dictionary;

public class ObjectStorage {

  public function ObjectStorage(parent:ObjectStorage=null) {
    this.parent = parent;
  }

  /**
   * Parent object table, parent object can't be
   * modified from here.
   */
  private var parent:ObjectStorage;

  /**
   * Object storage
   */
  private var map:Array = new Array();

  /**
   * Index of object storage. It needs for fast search ids by
   * object values
   */
  private var indexMap:Dictionary = new Dictionary(true);

  /**
   * Initializtion map. Objects can exeists in storage, but in might
   * be to initialized. When object pass initialization in puts in
   * this map.
   */
  private var initMap:Dictionary = new Dictionary(true);

  /**
   * @return Id for a new object.
   */
  public function generateId():int {

    var newId:int = Math.random() * 0xFFFFFFF;
    while (getObjectExists2(newId))
      newId = generateId();

    return newId;
  }

  /**
   * Register new object in reference table.
   *
   * @param object new object.
   * @return id of new object.
   */
  public function registerNewObject(object:Object):int {
    var id:int = generateId();
    registerObject(id, object);
    return id;
  }

  /**
   * Register any object in the reference table.
   *
   * @param id     object id
   * @param object object
   */
  public function registerObject(id:int, object:Object):void {
    map[id] = object;
    indexMap[object] = id;
  }

  /**
   * @param object any object
   * @return Id of object if it's allready registred else -1.
   */
  public function getObjectId(object:Object):int {
    if (parent != null) {
      var id_i:int = parent.getObjectId(object);
      if (id_i > -1)
        return id_i;
    }

    var id:Object = indexMap[object];
    return (id == null) ? -1 : int(id);
  }

  public function getObject(id:int):Object {
    var result:Object;
    if (parent != null)
      result = parent.getObject(id);
    if (result == null)
      result = map[id];
    return result;
  }

  public function getObjectExists(object:Object):Boolean {
    if (parent != null)
      return parent.getObjectExists(object) || (indexMap[object] != null);
    else return (indexMap[object] != null);
  }

  public function getObjectExists2(id:int):Boolean {
    if (parent != null)
      return parent.getObjectExists2(id) || (map[id] != null);
    else return (map[id] != null);
  }

  /**
   * Check object marked as initialized.
   *
   * @param object object
   * @return inializtion status.
   */
  public function getObjectInitialized(object:Object):Boolean {
    if (parent != null)
      return parent.getObjectInitialized(object) || initMap[object];
    else return initMap[object];
  }

  public function setObjectInitialized(object:Object):void {
    if (parent != null && parent.getObjectExists(object))
      parent.setObjectInitialized(object);
    else initMap[object] = true;
  }
}

}
