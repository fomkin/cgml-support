package com.yarrcad.cgml.net {

import flash.utils.ByteArray;

import org.flexunit.asserts.assertFalse;
import org.flexunit.asserts.assertTrue;

public class PacketStackTest {

    private static function v2b(v:Vector.<int>):ByteArray {
        var ba:ByteArray = new ByteArray();
        for each (var i:int in v)
            ba.writeByte(i);
        ba.position = 0;
        return ba;
    }

    [Test]
    public function testProcessData1():void {
        var bytes:ByteArray = v2b(new <int>[0, 0]);
        var o:PacketStack = new PacketStack();
        o.processInput(bytes);

        assertFalse("2 bytes is not packet.", o.size > 0);
    }

    [Test]
    public function testProcessData4():void {
        var bytes:ByteArray = v2b(new <int>[0, 0, 0, 0, 0, 0, 0, 0]);
        var o:PacketStack = new PacketStack();
        o.processInput(bytes);
        assertTrue("There is must be two packet with zero length.", o.size == 2);
    }

    [Test]
    public function testProcessData5():void {
        var bytes:ByteArray = new ByteArray();
        bytes.writeInt(2);
        bytes.writeByte(0);
        bytes.position = 0;

        var o:PacketStack = new PacketStack();
        o.processInput(bytes);

        assertFalse("Packet length is 2 bytes, but 1 byte written.", o.size > 0);
    }

    [Test]
    public function testProcessData6():void {
        var bytes:ByteArray = new ByteArray();
        bytes.writeInt(2);
        bytes.writeShort(0);
        bytes.position = 0;

        var o:PacketStack = new PacketStack();
        o.processInput(bytes);

        assertTrue(
                "Packet length is 2 bytes, and 2 byte written. This is correct packet",
                o.size == 1 && o.take().length == 2
        );
    }

    [Test]
    public function testProcessDataBroken():void {

        var o:PacketStack = new PacketStack();
        var bytes:ByteArray = new ByteArray();
        bytes.writeInt(7);
        bytes.writeBytes(v2b(new <int>[1, 2, 3, 4, 5, 6])); // пакет не заканчивается
        bytes.position = 0;

        o.processInput(bytes);

        bytes = new ByteArray()
        bytes.writeByte(0); // добиваем первый пакет
        bytes.writeInt(10);
        bytes.writeByte(0); // второй пакет не заканчивается
        bytes.position = 0;

        o.processInput(bytes);

        bytes = new ByteArray();
        bytes.writeBytes(v2b(new <int>[2, 3, 4, 5, 6, 7, 8, 9, 0])); // добиваем второй пакет
        bytes.position = 0;

        o.processInput(bytes);

        assertTrue(
                "Two packets was sent in different times. Query works correctly ",
                o.size == 2
        );
    }
}

}
