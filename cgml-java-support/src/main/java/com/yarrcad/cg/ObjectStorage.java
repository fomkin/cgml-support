/*
 * This file is part of the CGML Compiler
 *
 * Copyright(c) 2010 Aleksey Fomkin <aleksey.fomkin@gmail.com>
 * http://bitbucket.org/yelbota/cgml-compiler
 *
 * This file may be licensed under the terms of of the
 * GNU General Public License Version 3 (the ``GPL'').
 *
 * Software distributed under the License is distributed
 * on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the GPL for the specific language
 * governing rights and limitations.
 *
 * You should have received a copy of the GPL along with this
 * program. If not, go to http://www.gnu.org/licenses/gpl.html
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

package com.yarrcad.cg;

import java.util.HashMap;
import java.util.Random;

/**
 * Object storage is place, where readers and writers collect
 * declared objects.
 *
 * @author Aleksey Fomkin
 */
@SuppressWarnings("unused")
public final class ObjectStorage {

    /**
     * Object storage
     */
    private HashMap<Integer, Object> map = new HashMap<Integer, Object>();

    /**
     * Index of object storage. It needs for fast search ids by
     * object values
     */
    private HashMap<Object, Integer> indexMap = new HashMap<Object, Integer>();

    /**
     * Initializtion map. Objects can exeists in storage, but in might
     * be to initialized. When object pass initialization in puts in
     * this map.
     */
    private HashMap<Object, Boolean> initMap = new HashMap<Object, Boolean>();

    /**
     * Parent object table, parent object can't be
     * modified from here.
     */
    private ObjectStorage parent;

    public ObjectStorage(ObjectStorage parent) {
        this.parent = parent;
    }

    public ObjectStorage() {

    }

    /**
     * @return Id for a new object.
     */
    public int generateId() {

        Random random = new Random();

        int id = random.nextInt();
        while (getObjectExists(id))
            id = random.nextInt();

        return id;
    }

    /**
     * Register new object in reference table.
     *
     * @param object new object.
     * @return id of new object.
     */
    public int registerNewObject(Object object) {
        int id = generateId();
        registerObject(id, object);
        return id;
    }

    /**
     * Register any object in the reference table.
     *
     * @param id     object id
     * @param object object
     */
    public void registerObject(int id, Object object) {
        map.put(id, object);
        indexMap.put(object, id);
    }

    /**
     * @param object any object
     * @return Id of object if it's allready registred.
     */
    public int getObjectId(Object object) throws ObjectNotRegistred {
        if (parent != null) {
            Integer id = indexMap.get(object);
            if (id != null)
                return id;
        }

        Integer id = indexMap.get(object);
        if (id == null) {
            throw new ObjectNotRegistred();
        }
        else return id;
    }

    public Object getObject(int id) {
        if (parent != null && parent.getObjectExists(id))
            return parent.getObject(id);
        else return map.get(id);
    }

    public boolean getObjectExists(int id) {
        if (parent != null)
            return parent.getObjectExists(id) || map.containsKey(id);
        else return map.containsKey(id);
    }

    public boolean getObjectExists(Object object) {
        if (parent != null)
            return parent.getObjectExists(object) || map.containsValue(object);
        else return map.containsValue(object);
    }

    /**
     * Check object marked as initialized.
     *
     * @param object object
     * @return inializtion status.
     */
    public boolean getObjectInitialized(Object object) {
        if (parent != null && getObjectExists(object))
            return parent.getObjectInitialized(object);
        else {
            Boolean b = initMap.get(object);
            return (b == null) ? false : b;
        }
    }

    public void setObjectInitialized(Object object) {
        if (parent != null && getObjectExists(object))
            parent.setObjectInitialized(object);
        else initMap.put(object, true);
    }

    public class ObjectNotRegistred extends Exception {
    }
}
