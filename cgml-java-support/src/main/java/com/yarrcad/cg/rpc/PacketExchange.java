package com.yarrcad.cg.rpc;

public interface PacketExchange {
    byte[] send(byte[] data);
}
