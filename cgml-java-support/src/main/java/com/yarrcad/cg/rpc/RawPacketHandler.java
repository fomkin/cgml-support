package com.yarrcad.cg.rpc;

import java.io.IOException;

public interface RawPacketHandler {
    void process(byte[] packet) throws IOException;
}
