package com.yarrcad.cg.rpc;

public interface PacketExchangeAsync {
    void send(byte[] data, RawPacketHandler handler);
}
