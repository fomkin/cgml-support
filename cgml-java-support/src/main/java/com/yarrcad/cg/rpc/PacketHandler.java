package com.yarrcad.cg.rpc;

public interface PacketHandler<T> {
    void process(T packet);
}
