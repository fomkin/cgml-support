package com.yarrcad.cg.rpc;

public interface ServerInterfaceAsync {
    PacketExchangeAsync startPacketExchange();
}
