package com.yarrcad.cg.rpc.socket;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.LinkedList;

public class PacketQueue extends LinkedList<byte[]> {

	private byte[] buffer = new byte[0];
	private int knownPacketLength = -1;

	public void processInput(DataInputStream input) throws IOException {
		if (knownPacketLength > -1) {
			// We allready know packet length
			// So lets read out.
			int toRead = Math.min(input.available(), knownPacketLength - buffer.length);
			byte[] newBuffer = new byte[buffer.length + toRead];
			input.read(newBuffer, buffer.length, toRead);
			System.arraycopy(buffer, 0, newBuffer, 0, buffer.length);
			buffer = newBuffer;
			// Check packet length
			if (buffer.length >= knownPacketLength) {
				// Yeha! Packet complete.
				// Call user handler and reset the buffer.
				add(buffer);
				buffer = new byte[0];
				knownPacketLength = -1;
				processInput(input);
			}
		} else if (input.available() >= 4) {
			// Can read length of packet
			knownPacketLength = input.readInt();
			processInput(input);
		}
	}
}
