package com.yarrcad.cg.rpc;

import com.yarrcad.cg.ObjectStorage;

public abstract class AbstractSender<T> {

    private T serverInterface;
    private ObjectStorage objectStorage;

    protected T getServerInterface() {
        return serverInterface;
    }

    protected ObjectStorage getObjectStorage() {
        return objectStorage;
    }

    public AbstractSender(T serverInterface, ObjectStorage objectStorage) {
        this.serverInterface = serverInterface;
        this.objectStorage = objectStorage;
    }

}
