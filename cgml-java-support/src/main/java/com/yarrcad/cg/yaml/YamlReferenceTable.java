/*
 * This file is part of the CGML Compiler
 *
 * Copyright(c) 2010 Aleksey Fomkin <aleksey.fomkin@gmail.com>
 * http://bitbucket.org/yelbota/cgml-compiler
 *
 * This file may be licensed under the terms of of the
 * GNU General Public License Version 3 (the ``GPL'').
 *
 * Software distributed under the License is distributed
 * on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the GPL for the specific language
 * governing rights and limitations.
 *
 * You should have received a copy of the GPL along with this
 * program. If not, go to http://www.gnu.org/licenses/gpl.html
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

package com.yarrcad.cg.yaml;

import com.yarrcad.cg.ObjectStorage;
import org.yaml.snakeyaml.error.Mark;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

public class YamlReferenceTable {

    /**
     * Throws then reference is not found.
     */
    public class NoRefecenceException extends MarkedErrorException {
        public NoRefecenceException(String s, Mark mark) {
            super(s, mark);
        }
    }

    private class YamlReference {

        private Mark mark;
        private Object object;
        private String name;

        public YamlReference(Object object, String name, Mark mark) {
            this.object = object;
            this.mark = mark;
            this.name = name;
        }
    }

    /**
     * In the YAML readeds we have places, where declared objects are
     * used. User can write invalid code and we must tell him about it.
     */
    private List<YamlReference> yamlReferenceList = new LinkedList<YamlReference>();

    /**
     * Object storage
     */
    private ObjectStorage objectStorage;

    /**
     * Constructor
     * @param objectStorage true object table.
     */
    @SuppressWarnings("unused")
    public YamlReferenceTable(ObjectStorage objectStorage) {
        this.objectStorage = objectStorage;
    }

    /**
     * Uses in generaged yaml readers. Adds reference declaration.
     * @param object link to object from object storage.
     * @param mark place in the YAML document
     */
    @SuppressWarnings("unused")
    public void registerReference(Object object, String name, Mark mark) {
        yamlReferenceList.add(new YamlReference(object, name, mark));
    }

    /**
     * Reference validation.
     * @throws NoRefecenceException Then object follow the reference is not initialized.
     */
    @SuppressWarnings("unused")
    public void checkReferences() throws NoRefecenceException {
        for (YamlReference reference : yamlReferenceList) {
            // Check object is initialized
            if (!objectStorage.getObjectInitialized(reference.object)) {
                // If we don't know the name. It can't be but anyway lets check.
                if (reference.name == null) {
                    try {
                        reference.name = "@" + objectStorage.getObjectId(reference.object);
                    } catch (ObjectStorage.ObjectNotRegistred objectNotRegistred) {
                        throw new RuntimeException("YAML reference to unknown object. Please contact with developers.");
                    }
                }
                throw new NoRefecenceException(reference.name + " doesn't initialized.", reference.mark);
            }
        }
    }
}
