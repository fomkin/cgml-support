package com.yarrcad.cg.yaml;

import org.yaml.snakeyaml.error.Mark;

@SuppressWarnings("serial")
public class MarkedErrorException extends Throwable {
	private Mark mark; 
	public Mark getMark() {
		return mark;
	}
	
	public MarkedErrorException(String message, Mark mark) {
		super(message);
		this.mark = mark;
	}
}
