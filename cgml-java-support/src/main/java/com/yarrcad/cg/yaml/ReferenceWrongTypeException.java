package com.yarrcad.cg.yaml;

import org.yaml.snakeyaml.error.Mark;

/**
 * Throws when reference and target field has incompatible types.
 */
@SuppressWarnings("unused")
public class ReferenceWrongTypeException extends MarkedErrorException {

    public ReferenceWrongTypeException(String s, Mark mark) {
        super(s, mark);
    }
}