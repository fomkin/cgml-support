package com.yarrcad.cg.yaml;

import org.yaml.snakeyaml.error.Mark;

public class MarkedError {
	private Mark mark;
	private String message;
	
	public MarkedError(String message, Mark mark) {
		this.message = message;
		this.mark = mark;
	}

	public Mark getMark() {
		return mark;
	}

	public String getMessage() {
		return message;
	}
}
