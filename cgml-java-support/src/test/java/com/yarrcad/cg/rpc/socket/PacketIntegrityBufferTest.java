package com.yarrcad.cg.rpc.socket;

import org.junit.Test;

import java.io.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PacketIntegrityBufferTest {

    @Test
    public void testProcessData1() throws Exception {
        byte[] bytes = new byte[] { 0, 0 };
        PacketQueue o = new PacketQueue();
        o.processInput(new DataInputStream(new ByteArrayInputStream(bytes)));

        assertFalse("2 bytes is not packet.", o.size() > 0);
    }

    @Test
    public void testProcessData4() throws Exception {
        byte[] bytes = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 };
        PacketQueue o = new PacketQueue();
        o.processInput(new DataInputStream(new ByteArrayInputStream(bytes)));

        assertTrue("There is must be two packet with zero length.", o.size() == 2);
    }

    @Test
    public void testProcessData5() throws Exception {
        ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream(2);
        DataOutput dataOutput = new DataOutputStream(byteOutputStream);
        dataOutput.writeInt(2);
        dataOutput.writeByte(0);

        byte[] bytes = byteOutputStream.toByteArray();
        PacketQueue o = new PacketQueue();
        o.processInput(new DataInputStream(new ByteArrayInputStream(bytes)));

        assertFalse("Packet length is 2 bytes, but 1 byte written.", o.size() > 0);
    }

    @Test
    public void testProcessData6() throws Exception {
        ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream(2);
        DataOutput dataOutput = new DataOutputStream(byteOutputStream);
        dataOutput.writeInt(2);
        dataOutput.writeShort(0);

        byte[] bytes = byteOutputStream.toByteArray();
        PacketQueue o = new PacketQueue();
        o.processInput(new DataInputStream(new ByteArrayInputStream(bytes)));

        assertTrue(
                "Packet length is 2 bytes, and 2 byte written. This is corrent packet",
                o.size() == 1 && o.get(0).length == 2
        );
    }

    @Test
    public void testProcessDataBroken() throws Exception {

        PacketQueue o = new PacketQueue();

        ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream(10);
        DataOutput dataOutput = new DataOutputStream(byteOutputStream);
        dataOutput.writeInt(7);
        dataOutput.writeBytes("123456"); // пакет не заканчивается

        byte[] bytes = byteOutputStream.toByteArray();
        o.processInput(new DataInputStream(new ByteArrayInputStream(bytes)));

        byteOutputStream = new ByteArrayOutputStream(6);
        dataOutput = new DataOutputStream(byteOutputStream);
        dataOutput.writeByte(0); // добиваем первый пакет
        dataOutput.writeInt(10);
        dataOutput.writeByte(0); // второй пакет не заканчивается

        bytes = byteOutputStream.toByteArray();
        o.processInput(new DataInputStream(new ByteArrayInputStream(bytes)));

        byteOutputStream = new ByteArrayOutputStream(9);
        dataOutput = new DataOutputStream(byteOutputStream);
        dataOutput.writeBytes("234567890"); // добиваем второй пакет

        bytes = byteOutputStream.toByteArray();
        o.processInput(new DataInputStream(new ByteArrayInputStream(bytes)));

        assertTrue(
                "Two packets was sent in different times. Query works correctly ",
                o.size() == 2
        );
    }

}
